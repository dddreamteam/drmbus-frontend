import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from '@angular/router';
import {LayoutModule} from '@angular/cdk/layout';
import {MaterialModule} from './material.module';
import {SidebarModule} from './base-components/sidebar/sidebar.module';
import {RoutesPageModule} from './pages/routes-page/routes-page.module';
import {MessagesPageModule} from './pages/messages-page/messages-page.module';
import {HomePageModule} from './pages/home-page/home-page.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NotfoundPageComponent } from './pages/notfound-page/notfound-page.component';
import { PageLoadingModule } from './base-components/page-loading/page-loading.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DividerModule } from './base-components/divider/divider.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import { LoginPageModule } from './pages/login-page/login-page.module';
import {CompanyPageModule} from './pages/company-page/company-page.module';

@NgModule({
  declarations: [
    AppComponent,
    NotfoundPageComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    FlexLayoutModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    LayoutModule,
    AppRoutingModule,
    HttpClientModule,
    SidebarModule,
    RoutesPageModule,
    MessagesPageModule,
    HomePageModule,
    PageLoadingModule,
    DividerModule,
    LoginPageModule,
    CompanyPageModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
