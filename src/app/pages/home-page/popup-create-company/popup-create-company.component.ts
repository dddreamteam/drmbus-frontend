import {Component, Inject, Input, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {CompanyServiceImpl} from '../../../services/service-impl/company-impl.service';
import {Company} from '../../../shared/entities.service';
import {CompanyDetailsComponent} from '../../../components/company-details/company-details.component';

@Component({
  selector: 'app-popup-create-company',
  templateUrl: './popup-create-company.component.html',
  styleUrls: ['./popup-create-company.component.scss']
})
export class PopupCreateCompanyComponent implements OnInit {

  constructor(
    private dialogCreateCompany: MatDialogRef<PopupCreateCompanyComponent>,
    private companyService: CompanyServiceImpl,
  ) {
  }
  close(): void {
    this.dialogCreateCompany.close();
  }

  ngOnInit(): void {
  }
  save(company: Company) {

    console.log('Company from popup', company );
    /*this.companyService.save();*/
    this.dialogCreateCompany.close();
  }
}
