import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupCreateCompanyComponent } from './popup-create-company.component';
import {MaterialModule} from '../../../material.module';
import {CompanyDetailsModule} from '../../../components/company-details/company-details.module';
import {ToolbarModule} from '../../../base-components/toolbar/toolbar.module';


@NgModule({
  declarations: [PopupCreateCompanyComponent],
  imports: [
    CommonModule,
    MaterialModule,
    CompanyDetailsModule,
    ToolbarModule
  ],
  exports: [
    PopupCreateCompanyComponent
  ]
})
export class PopupCreateCompanyModule { }
