import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCreateCompanyComponent } from './popup-create-company.component';

describe('PopupCreateCompanyComponent', () => {
  let component: PopupCreateCompanyComponent;
  let fixture: ComponentFixture<PopupCreateCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCreateCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCreateCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
