import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CardCompanyComponent} from './card-company.component';
import {MaterialModule} from '../../../material.module';
import { RouterModule } from '@angular/router';




@NgModule({
  declarations: [
    CardCompanyComponent
  ],
  exports: [
    CardCompanyComponent
  ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule,
    ]
})
export class CardCompanyModule { }
