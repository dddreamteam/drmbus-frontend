import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { UserServiceImpl } from '../../../services/service-impl/user-impl.service';
import { Company } from '../../../shared/entities.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-card-company',
  templateUrl: './card-company.component.html',
  styleUrls: ['./card-company.component.scss']
})
export class CardCompanyComponent implements OnInit {

  private company: Company;

  @Output()  companyEmitter: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  @Input()
  public set _company(company: Company) {
    this.company = company;
  }
  public get _company(): Company {
    this.companyEmitter.emit(this.company.id)
    return this.company;
  }

  ngOnInit(): void {
  }

}
