import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardCompanyModule } from './card-company/card-company.module';
import { MaterialModule } from '../../material.module';
import { HomePageComponent } from './home-page.component';
import { FlexModule } from '@angular/flex-layout';
import { DividerModule } from '../../base-components/divider/divider.module';
import { ToolbarModule } from '../../base-components/toolbar/toolbar.module';
import { VoyageSearchPanelModule } from './voyage-search-panel/voyage-search-panel.module';
import {CompanyDetailsModule} from '../../components/company-details/company-details.module';
import {PopupCreateCompanyModule} from './popup-create-company/popup-create-company.module';


@NgModule({
  declarations: [
    HomePageComponent,
  ],
  exports: [
    HomePageComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    CardCompanyModule,
    FlexModule,
    DividerModule,
    ToolbarModule,
    VoyageSearchPanelModule,
    CompanyDetailsModule,
    PopupCreateCompanyModule
  ]
})
export class HomePageModule {
}
