import { Component, OnInit } from '@angular/core';
import { UserServiceImpl } from '../../services/service-impl/user-impl.service';
import { User } from '../../shared/entities.service';
import {PopupCreateCompanyComponent} from './popup-create-company/popup-create-company.component';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public user: User;

  constructor(private userServiceImpl: UserServiceImpl,
              private dialog: MatDialog) { }

  ngOnInit(): void {

    this.userServiceImpl.userObs.subscribe(user => {
      this.user = user;
    });
  }

  createCompany() {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.disableClose = true;
    this.dialog.open(PopupCreateCompanyComponent, matDialogConfig);
  }
}
