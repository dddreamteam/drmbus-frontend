import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoyageSearchPanelComponent } from './voyage-search-panel.component';
import { MaterialModule } from '../../../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexModule } from '@angular/flex-layout';
import { PageLoadingModule } from '../../../base-components/page-loading/page-loading.module';
import { VoyageTableStyleDirective } from './voyage-table.directive';


@NgModule({
  declarations: [
    VoyageSearchPanelComponent,
    VoyageTableStyleDirective
  ],
  exports: [
    VoyageSearchPanelComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexModule,
    PageLoadingModule,
  ]
})
export class VoyageSearchPanelModule {
}
