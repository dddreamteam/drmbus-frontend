import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appVoyageTableStyle]'
})
export class VoyageTableStyleDirective implements OnInit {

  @Input()
  status = 'WEEK_MORE';

  constructor(private el: ElementRef, private r: Renderer2) {}
  ngOnInit() {
    if (this.status.match('WEEK_MORE')) {
      this.r.setStyle(this.el.nativeElement, 'background-color', '#fff');
    } else if (this.status.match('WEEK_TO')) {
      this.r.setStyle(this.el.nativeElement, 'background-color', '#ffebee');
    } else if (this.status.match('DAY_TO')) {
      this.r.setStyle(this.el.nativeElement, 'background-color', '#ffcdd2');
    } else if (this.status.match('ON_THE_WAY')) {
      this.r.setStyle(this.el.nativeElement, 'background-color', '#bbdefb');
    } else if (this.status.match('COMPLETE')) {
      this.r.setStyle(this.el.nativeElement, 'background-color', '#c8e6c9');
    } else if (this.status.match('CANCELED')) {
      this.r.setStyle(this.el.nativeElement, 'background-color', '#cfd8dc');
    }
  }

}
