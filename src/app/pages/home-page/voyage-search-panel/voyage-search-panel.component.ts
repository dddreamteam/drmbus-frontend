import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Seat, User, Voyage } from '../../../shared/entities.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { from, Observable } from 'rxjs';
import { distinct, map, mergeMap, startWith, toArray } from 'rxjs/operators';
import { VoyageService } from '../../../services/service-impl/voyage.service';
import { UserServiceImpl } from '../../../services/service-impl/user-impl.service';
import { PageLoaderService } from '../../../services/service-impl/page-loader.service';

@Component({
  selector: 'app-voyage-search-panel',
  templateUrl: './voyage-search-panel.component.html',
  styleUrls: [ './voyage-search-panel.component.scss' ],
})
export class VoyageSearchPanelComponent implements OnInit {
  public displayedColumns: string[] =
    [ 'departureDate', 'departureStation', 'arrivalStation', 'available', 'sold', 'reserve', 'unavailable' ];

  public dataSource: MatTableDataSource<Voyage>;

  public filteredArrivalStations: Observable<string[]>;
  public filteredDepartureStations: Observable<string[]>;

  private arrivalStations: string[];
  private departureStations: string[];
  public searchForm: FormGroup;
  public departureDate = '';
  public arrivalStation = '';
  public departureStation = '';
  public voyages: Voyage[] = [];
  public user: User;

  constructor(private userServiceImpl: UserServiceImpl,
              private voyageService: VoyageService,
              public loader: PageLoaderService) {
  }

  ngOnInit() {
    this.loader.show();


    this.userServiceImpl.userObs.subscribe(user => this.user = user);

    this.searchForm = new FormGroup({
      arrivalStation: new FormControl('', Validators.pattern('^[a-zA-Z ]+$')),
      departureStation: new FormControl('', Validators.pattern('^[a-zA-Z ]+$')),
      departureDate: new FormControl('')
    });

    this.voyageService.fetchAllByUUID(this.user.uuid).subscribe(vs => {
      this.voyages = vs;
      this.dataSource = new MatTableDataSource<Voyage>(vs);
      this.dataFilterPredicate();
      this.autoCompleteFilter(vs);
      this.loader.hide();
    });

  }

  applyFilter() {
    const date = this.searchForm.get('departureDate').value;
    const ds = this.searchForm.get('departureStation').value;
    const as = this.searchForm.get('arrivalStation').value;

    this.departureDate = (date === null || date === '') ? '' : date.toDateString();
    this.arrivalStation = as === null ? '' : as;
    this.departureStation = ds === null ? '' : ds;

    const filterValue = this.departureDate + '$' + this.departureStation + '$' + this.arrivalStation;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private arrivalStationsFilter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.arrivalStations.filter(city => city.toLowerCase().includes(filterValue));
  }

  private departureStationsFilter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.departureStations.filter(city => city.toLowerCase().includes(filterValue));
  }


  private dataFilterPredicate() {
    /* Filter predicate used for filtering table per different columns
   *  */
    this.dataSource.filterPredicate =
      (data: Voyage, filters: string) => {
        // split string per '$' to array
        const filterArray = filters.split('$');
        const departureDate = filterArray[0];
        const departureStation = filterArray[1];
        const arrivalStation = filterArray[2];

        // Fetch data from row
        const matchFilter = [];
        const columnDepartureDate = new Date (data.departureDateTime);
        const columnDepartureStation = data.route.stations[0].address.city;
        const columnArrivalStation = data.route.stations[data.route.stations.length - 1].address.city;

        // verify fetching data by our searching values
        const customFilterDD = columnDepartureDate.toDateString().toLowerCase().includes(departureDate);
        const customFilterDS = columnDepartureStation.toLowerCase().includes(departureStation);
        const customFilterAS = columnArrivalStation.toLowerCase().includes(arrivalStation);

        // push boolean values into array
        matchFilter.push(customFilterDD);
        matchFilter.push(customFilterDS);
        matchFilter.push(customFilterAS);

        // return true if all values in array is true
        // else return false
        return matchFilter.every(Boolean);
      };
  }

  private autoCompleteFilter(vs: Voyage[]) {
    from(vs).pipe( // create stream from voyages
      map((voyage) => voyage.route), // map each voyage to route
      map((route) => route.stations), // map each route to stations array
      mergeMap(stations => stations), // Maps each value to an Observable, then flattens all of these inner Observables using
      map(station => station.address.city), // map each station to station name
      /* tslint:disable:max-line-length */
      distinct(), // Returns an Observable that emits all items emitted by the source Observable that are distinct by comparison from previous items.
      toArray() // collect all stations name in array
    ).subscribe(result => {
      this.arrivalStations = [ ...result ];
      this.departureStations = [ ...result ];
    });

    this.filteredArrivalStations = this.searchForm.get('arrivalStation').valueChanges
      .pipe(
        startWith(''),
        map(city => this.arrivalStationsFilter(city))
      );
    this.filteredDepartureStations = this.searchForm.get('departureStation').valueChanges
      .pipe(
        startWith(''),
        map(city => this.departureStationsFilter(city))
      );
  }

  availableSeats(seats: Seat[]): number {
    return seats.filter(seat => seat.status.match('AVAILABLE')).length;
  }

  unavailableSeats(seats: Seat[]): number {
    return seats.filter(seat => seat.status.match('UNAVAILABLE')).length;
  }

  soldSeats(seats: Seat[]): number {
    return seats.filter(seat => seat.status.match('SOLD')).length;
  }

  reserveSeats(seats: Seat[]): number {
    return seats.filter(seat => seat.status.match('RESERVE')).length;
  }
}
