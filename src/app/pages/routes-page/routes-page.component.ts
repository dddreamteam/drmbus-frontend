import {Component, OnInit} from '@angular/core';
import {DRMRoute, User} from '../../shared/entities.service';
import {DRMRouteServiceImpl} from '../../services/service-impl/drmroute-impl.service';
import {UserServiceImpl} from '../../services/service-impl/user-impl.service';
import {PageLoaderService} from '../../services/service-impl/page-loader.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {PopupCreateRouteComponent} from './popup-create-route/popup-create-route.component';


@Component({
  selector: 'app-routes-page',
  templateUrl: './routes-page.component.html',
  styleUrls: ['./routes-page.component.scss']
})
export class RoutesPageComponent implements OnInit {
  public routes: DRMRoute[];
  private user: User;

  constructor(
    private drmRouteService: DRMRouteServiceImpl,
    private userService: UserServiceImpl,
    public loader: PageLoaderService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.userService.userObs.subscribe(user => this.user = user);
    this.fetchAllRoutesByUserId();
    this.loader.show();
  }

  fetchAllRoutesByUserId() {

    this.drmRouteService.fetchAllByUserId(this.user.uuid).subscribe(
      routes => {
        this.drmRouteService.updateRouteList(routes);
        this.drmRouteService.routeObs.subscribe(r => this.routes = r);
        // this.routes = routes;
        this.loader.hide();
      }
    );
  }

  createRoute() {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.disableClose = true;
    this.dialog.open(PopupCreateRouteComponent, matDialogConfig);
  }
}
