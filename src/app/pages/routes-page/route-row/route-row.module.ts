import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouteRowComponent} from './route-row.component';
import {MaterialModule} from '../../../material.module';
import {RowStyleDirective} from './row-style.directive';


@NgModule({
  declarations: [
    RouteRowComponent,
    RowStyleDirective
  ],
  exports: [
    RouteRowComponent,
    RowStyleDirective
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ]
})
export class RouteRowModule {
}
