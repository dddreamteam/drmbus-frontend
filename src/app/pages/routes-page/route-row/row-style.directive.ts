import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appRowStyleDirective]'
})
export class RowStyleDirective implements OnInit {

  // tslint:disable-next-line:jsdoc-format
  /**
   * @rowsHeight - height of rows container
   * @rows - quantity of rows
   */
  @Input() rowHeight: {rowsHeight: number, rows: number};

  constructor(private el: ElementRef, private r: Renderer2) {}

  ngOnInit(): void {
    this.r.setStyle(
     this. el.nativeElement,
      'height',
      (this.rowHeight.rowsHeight / this.rowHeight.rows) + 'px'
    );
  }
}
