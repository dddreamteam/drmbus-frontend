import {Component, Input, OnInit} from '@angular/core';
import {DRMRoute} from '../../../shared/entities.service';
import {DRMRouteServiceImpl} from '../../../services/service-impl/drmroute-impl.service';
import {debuglog} from 'util';


@Component({
  selector: 'app-base-route',
  templateUrl: './route-row.component.html',
  styleUrls: ['./route-row.component.scss']
})
export class RouteRowComponent implements OnInit {

  public route: DRMRoute;
  public indexRoute: number;
  public collapsedHeight = 150;
  private routes: DRMRoute[];

  @Input()
  public set setRoute(route: DRMRoute) {
    this.route = route;
  }

  @Input()
  public set setIndexRoute(indexRoute: number) {
    this.indexRoute = indexRoute;
  }

  constructor(
    private drmRouteService: DRMRouteServiceImpl
  ) {
  }

  ngOnInit(): void {
  }

  deleteRoute() {
    this.drmRouteService.delete(this.route.id).subscribe(
      next => {}, error => {},
      () => {
        this.drmRouteService.routeObs.subscribe(routes => this.routes = routes);
        const newRoutes = [...this.routes];
        for (let i = 0; i < this.routes.length; i++) {
          if (newRoutes[i].id === this.route.id) {
            newRoutes.splice(i, 1);
          }
        }
        this.drmRouteService.updateRouteList(newRoutes);
      }
    );
  }
}
