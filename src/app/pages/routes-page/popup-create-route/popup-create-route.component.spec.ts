import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupCreateRouteComponent } from './popup-create-route.component';

describe('PopupCreateRouteComponent', () => {
  let component: PopupCreateRouteComponent;
  let fixture: ComponentFixture<PopupCreateRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupCreateRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCreateRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
