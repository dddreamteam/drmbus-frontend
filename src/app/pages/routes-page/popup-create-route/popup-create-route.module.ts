import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PopupCreateRouteComponent} from './popup-create-route.component';
import {MaterialModule} from '../../../material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [PopupCreateRouteComponent],
  exports: [
    PopupCreateRouteComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PopupCreateRouteModule {
}
