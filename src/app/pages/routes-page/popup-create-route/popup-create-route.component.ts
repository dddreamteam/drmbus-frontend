import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {DRMRouteServiceImpl} from '../../../services/service-impl/drmroute-impl.service';
import {Address, DRMRoute, Schedule, Station, User} from '../../../shared/entities.service';
import {UserServiceImpl} from '../../../services/service-impl/user-impl.service';
import {ScheduleServiceImpl} from '../../../services/service-impl/schedule-impl.service';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {MyValidators} from './my.validators';
import {StationServiceImpl} from '../../../services/service-impl/station-impl.service';
import {from, of} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-popup-create-route',
  templateUrl: './popup-create-route.component.html',
  styleUrls: ['./popup-create-route.component.scss']
})
export class PopupCreateRouteComponent implements OnInit {

  drmRoute: DRMRoute = {
    companyId: 0,
    deepSales: 0,
    firstVoyageDate: new Date(),
    isActive: false,
    number: '',
    seatCount: 0,
    stations: []
  };
  user: User = {
    companies: [],
    username: ''
  };
  schedule: Schedule = {
    friday: false,
    interval: null,
    monday: false,
    routeDTO: undefined,
    saturday: false,
    sunday: false,
    thursday: false,
    tuesday: false,
    type: '',
    wednesday: false
  };

  private routes: DRMRoute[];
  stations: Station[];
  form: FormGroup;

  constructor(
    private dialogCreateRoute: MatDialogRef<PopupCreateRouteComponent>,
    private drmRouteService: DRMRouteServiceImpl,
    private userService: UserServiceImpl,
    private scheduleService: ScheduleServiceImpl,
    private stationService: StationServiceImpl
  ) {
  }

  ngOnInit(): void {
    // this.userService.fetchUserByUsername('user@gmail.com').subscribe(user => {
    //   this.user = user;
    // });
    this.userService.userObs.subscribe(user => this.user = user);
    this.drmRouteService.routeObs.subscribe(r => this.routes = r);
    this.stationService.fetchAll().subscribe(stations => this.stations = stations);

    this.form = new FormGroup({
      companyId: new FormControl('', Validators.required),
      number: new FormControl('', Validators.required),
      firstVoyageDate: new FormControl('', Validators.required),
      seatCount: new FormControl('', [Validators.required, Validators.min(1), Validators.max(100)]),
      deepSales: new FormControl('', [Validators.required, Validators.min(30), Validators.max(45)]),
      stations: new FormArray([], [Validators.required, MyValidators.minLengthArray(2)]),
      scheduleType: new FormControl('', Validators.required),
      scheduleInterval: new FormControl(''),
      scheduleMonday: new FormControl(''),
      scheduleTuesday: new FormControl(''),
      scheduleWednesday: new FormControl(''),
      scheduleThursday: new FormControl(''),
      scheduleFriday: new FormControl(''),
      scheduleSaturday: new FormControl(''),
      scheduleSunday: new FormControl('')
    });
  }

  submitRoute() {
    if (this.form.valid) {
      // до сохранения в базу без рассписания маршрут должен быть не активен
      // что бы не генерировать ошибочных вояжей
      const formData = {...this.form.value};
      this.drmRoute.companyId = formData.companyId;
      this.drmRoute.deepSales = formData.deepSales;
      this.drmRoute.firstVoyageDate = formData.firstVoyageDate;
      this.drmRoute.number = formData.number;
      this.drmRoute.seatCount = formData.seatCount;
      this.drmRoute.stations = formData.stations;
      this.drmRoute.isActive = false;

      from(this.drmRoute.stations).pipe(
        map(station => {
          const stationFilter = this.stations.filter(st => {
            return st.id === station.id;
          });
          if (stationFilter[0]) {
            station = stationFilter[0];
          } else {
            console.log('errStationFilter', stationFilter);
          }
        })
      ).subscribe();

      this.schedule.routeDTO = this.drmRoute;
      this.schedule.type = formData.scheduleType;
      if (this.schedule.type === 'INTERVAL') {
        this.schedule.interval = formData.scheduleInterval;
      }
      if (this.schedule.type === 'WEEK') {
        this.schedule.monday = formData.scheduleMonday !== '';
        this.schedule.tuesday = formData.scheduleTuesday !== '';
        this.schedule.wednesday = formData.scheduleWednesday !== '';
        this.schedule.thursday = formData.scheduleThursday !== '';
        this.schedule.friday = formData.scheduleFriday !== '';
        this.schedule.saturday = formData.scheduleSaturday !== '';
        this.schedule.sunday = formData.scheduleSunday !== '';
      }

      console.log('scheduleBeforeSave', this.schedule);

      this.scheduleService.save(this.schedule).subscribe(
        schedule => {
          console.log('scheduleFromDB', schedule);
          this.routes.push(schedule.routeDTO);
          this.drmRouteService.updateRouteList(this.routes);
        }
      );

      this.dialogCreateRoute.close();

      // na vsyakiy
      // this.drmRouteService.save(this.drmRoute).subscribe(
      //   route => {
      //     console.log('route', route);
      //     this.schedule.routeDTO = route;
      //     this.schedule.type = formData.scheduleType;
      //     if (this.schedule.type === 'INTERVAL') {
      //       this.schedule.interval = formData.scheduleInterval;
      //     }
      //     if (this.schedule.type === 'WEEK') {
      //       this.schedule.monday = formData.scheduleMonday;
      //       this.schedule.tuesday = formData.scheduleTuesday;
      //       this.schedule.wednesday = formData.scheduleWednesday;
      //       this.schedule.thursday = formData.scheduleThursday;
      //       this.schedule.friday = formData.scheduleFriday;
      //       this.schedule.saturday = formData.scheduleSaturday;
      //       this.schedule.sunday = formData.scheduleSunday;
      //     }
      //
      //     console.log('scheduleBeforeSave', this.schedule);
      //
      //     this.scheduleService.save(this.schedule).subscribe(
      //       schedule => {
      //         console.log('scheduleFromDB', schedule);
      //       }
      //     );
      //   }
      // );
    } else {
      console.log('err', 'form is invalid');
    }
  }

  addStation() {
    const stationControl = new FormControl('', Validators.required);
    (this.form.get('stations') as FormArray).push(stationControl);
  }

}
