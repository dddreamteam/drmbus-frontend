import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material.module';
import {RouteRowModule} from './route-row/route-row.module';
import {RoutesPageComponent} from './routes-page.component';
import {ToolbarModule} from '../../base-components/toolbar/toolbar.module';
import {PageLoadingModule} from '../../base-components/page-loading/page-loading.module';
import {PopupCreateRouteModule} from './popup-create-route/popup-create-route.module';


@NgModule({
  declarations: [
    RoutesPageComponent
  ],
  exports: [
    RoutesPageComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouteRowModule,
    ToolbarModule,
    PageLoadingModule,
    PopupCreateRouteModule
  ]
})
export class RoutesPageModule {
}
