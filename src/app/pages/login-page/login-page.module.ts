import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page.component';
import { MaterialModule } from '../../material.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ LoginPageComponent ],
  exports: [
    LoginPageComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
  ]
})
export class LoginPageModule { }
