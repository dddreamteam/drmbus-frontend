import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserServiceImpl } from '../../services/service-impl/user-impl.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private authService: AuthService,
              private router: Router,
              private userServiceImpl: UserServiceImpl,
  ) { }


  ngOnInit(): void {
    this.loginForm = new FormGroup({
        username: new FormControl(''),
        password: new FormControl('')
      }
    );
  }

  login() {

    const body = new URLSearchParams();
    body.set('username', this.loginForm.value.username);
    body.set('password', this.loginForm.value.password);
    body.set('grant_type', 'password');

    this.authService.getToken(body).subscribe(
      respond => {
        this.fetchUser();
      },
      error => {
        console.log(error);
        this.authService.logout();
      }
    );
  }

  private fetchUser() {
    const userObservable = this.userServiceImpl.fetchUserByUsername(this.loginForm.value.username);
    userObservable.subscribe(
      user => {
        this.userServiceImpl.updateUser(user);
        this.authService.login();
        this.router.navigate(['/']);
        console.log('user', user);
      }
    );
  }
}
