import {Component, OnInit} from '@angular/core';
import {DRMRoute, Station} from '../../../shared/entities.service';
import {CompanyServiceImpl} from '../../../services/service-impl/company-impl.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-company-routes',
  templateUrl: './company-routes.component.html',
  styleUrls: ['./company-routes.component.scss']
})
export class CompanyRoutesComponent implements OnInit {
  public routes: DRMRoute[];

  constructor(private companyService: CompanyServiceImpl,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((param) => {
      if (param.id) {
        this.fetchAllRoutesByCompanyId(+param.id);
      }
    });
  }

  fetchAllRoutesByCompanyId(id: number) {
    this.companyService.fetchAllRoutesByCompanyId(id).subscribe(
      routes => {
        this.routes = routes;
      }
    );
  }
}
