import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyRoutesComponent } from './company-routes.component';

describe('CompanyRoutesComponent', () => {
  let component: CompanyRoutesComponent;
  let fixture: ComponentFixture<CompanyRoutesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyRoutesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
