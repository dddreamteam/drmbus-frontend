import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyRoutesComponent } from './company-routes.component';
import {RouteRowModule} from '../../routes-page/route-row/route-row.module';
import {MaterialModule} from '../../../material.module';
import {MatChipsModule} from '@angular/material/chips';



@NgModule({
  declarations: [CompanyRoutesComponent],
  exports: [
    CompanyRoutesComponent
  ],
  imports: [
    CommonModule,
    RouteRowModule,
    MaterialModule,
    MatChipsModule
  ]
})
export class CompanyRoutesModule { }
