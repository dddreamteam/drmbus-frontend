import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CompanyDetailsModule} from '../../components/company-details/company-details.module';
import {CompanyPageComponent} from './company-page.component';
import {DividerModule} from '../../base-components/divider/divider.module';
import {CompanyRoutesModule} from './company-routes/company-routes.module';
import { ToolbarModule } from '../../base-components/toolbar/toolbar.module';



@NgModule({
  declarations: [CompanyPageComponent],
    imports: [
        CommonModule,
        CompanyDetailsModule,
        DividerModule,
        CompanyRoutesModule,
        ToolbarModule
    ]
})
export class CompanyPageModule { }
