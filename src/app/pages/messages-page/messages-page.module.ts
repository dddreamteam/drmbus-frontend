import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MessageComponent} from './message/message.component';
import {MessageModule} from './message/message.module';
import {MessagesPageComponent} from './messages-page.component';


@NgModule({
  declarations: [
    MessagesPageComponent
  ],
  exports: [
    MessagesPageComponent
  ],
  imports: [
    CommonModule,
    MessageModule,
  ]
})
export class MessagesPageModule {
}
