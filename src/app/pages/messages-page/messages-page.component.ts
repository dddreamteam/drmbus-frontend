import { Component, OnInit } from '@angular/core';
import { Message, User } from '../../shared/entities.service';


@Component({
  selector: 'app-message-page',
  templateUrl: './messages-page.component.html',
  styleUrls: ['./messages-page.component.scss']
})
export class MessagesPageComponent implements OnInit {
  messageList: Message[];

  constructor() { }

  ngOnInit(): void {
    this.messageList = [{text : 'message1'}, {text : 'message2'}, {text : 'message3'}];

  }

}
