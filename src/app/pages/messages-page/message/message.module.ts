import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../../material.module';
import {MessageComponent} from './message.component';

@NgModule({
  declarations: [MessageComponent],
  exports: [MessageComponent],
  imports: [
    CommonModule,
    MaterialModule,
  ]
})
export class MessageModule { }
