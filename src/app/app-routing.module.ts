import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {RoutesPageComponent} from './pages/routes-page/routes-page.component';
import {MessagesPageComponent} from './pages/messages-page/messages-page.component';
import { NotfoundPageComponent } from './pages/notfound-page/notfound-page.component';
import { CompanyPageComponent } from './pages/company-page/company-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'routes', component: RoutesPageComponent },
  { path: 'companies/:id', component: CompanyPageComponent},
  { path: 'messages', component: MessagesPageComponent },
  { path: '404', component: NotfoundPageComponent },
  { path: 'login', component: LoginPageComponent },



  // always last
  { path: '**', redirectTo: '/404' },
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  }
)

export class AppRoutingModule {
}
