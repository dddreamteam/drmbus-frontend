import {Schedule} from '../shared/entities.service';
import {Observable} from 'rxjs';

export interface ScheduleService {
  save: (route: Schedule) => Observable<Schedule>;
  update: (route: Schedule) => Observable<Schedule>;
  fetch: (id: number) => Observable<Schedule>;
  delete: (id: number) => Observable<void>;
}
