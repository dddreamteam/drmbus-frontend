import {Station} from '../shared/entities.service';
import {Observable} from 'rxjs';

export interface StationService {
  save: (route: Station) => Observable<Station>;
  update: (route: Station) => Observable<Station>;
  fetch: (id: number) => Observable<Station>;
  delete: (id: number) => Observable<void>;
  fetchAll: () => Observable<Station[]>;
}
