import { Injectable } from '@angular/core';
import {Address, Company} from '../shared/entities.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  save: (address: Address) => Observable<Address>;
  update: (address: Address) => Observable<Address>;
  fetch: (id: number) => Observable<Address>;
  delete: (id: number) => Observable<void>;
}
