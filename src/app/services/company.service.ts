import {Company, DRMRoute, User} from '../shared/entities.service';
import { Observable } from 'rxjs';

export interface CompanyService {
  save: (company: Company) => Observable<Company>;
  update: (company: Company) => Observable<Company>;
  fetch: (id: number) => Observable<Company>;
  delete: (id: number) => Observable<void>;
  fetchAllByUserUUID: (uuid: string) => Observable<Company[]>;
  fetchAllRoutesByCompanyId: (id: number) => Observable<DRMRoute[]>;
}

