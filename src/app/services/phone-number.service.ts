import { Injectable } from '@angular/core';
import {Company, PhoneNumber} from '../shared/entities.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhoneNumberService {
  save: (phoneNumber: PhoneNumber) => Observable<PhoneNumber>;
  update: (phoneNumber: PhoneNumber) => Observable<PhoneNumber>;
  fetch: (id: number) => Observable<PhoneNumber>;
  delete: (id: number) => Observable<void>;
}
