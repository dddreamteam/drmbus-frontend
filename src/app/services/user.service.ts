import { User } from '../shared/entities.service';
import { Observable } from 'rxjs';

export interface UserService {
  fetchUserById: (uuid: string) => Observable<User>;
  fetchUserByUsername: (username: string) => Observable<User>;
}

