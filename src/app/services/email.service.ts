import { Injectable } from '@angular/core';
import { Email} from '../shared/entities.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  save: (email: Email) => Observable<Email>;
  update: (email: Email) => Observable<Email>;
  fetch: (id: number) => Observable<Email>;
  delete: (id: number) => Observable<void>;
}
