import {DRMRoute} from '../shared/entities.service';
import {Observable} from 'rxjs';

export interface DRMRouteService {
  save: (route: DRMRoute) => Observable<DRMRoute>;
  update: (route: DRMRoute) => Observable<DRMRoute>;
  fetch: (id: number) => Observable<DRMRoute>;
  delete: (id: number) => Observable<void>;
  fetchAllByUserId: (uuid: string) => Observable<DRMRoute[]>;
}
