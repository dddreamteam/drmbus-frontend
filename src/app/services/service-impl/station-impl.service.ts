import {HttpClient} from '@angular/common/http';
import {Station} from '../../shared/entities.service';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {StationService} from '../station.service';

const URL = 'http://localhost:9006';

@Injectable({
  providedIn: 'root'
})
export class StationServiceImpl implements StationService {

  constructor(private http: HttpClient) {
  }

  save(station: Station): Observable<Station> {
    return this.http.post<Station>(`${URL}/api/stations`, station).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  update(station: Station): Observable<Station> {
    return this.http.put<Station>(`${URL}/api/stations`, station).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  fetch(id: number): Observable<Station> {
    return this.http.get<Station>(`${URL}/api/stations/${id}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${URL}/api/stations/${id}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  fetchAll(): Observable<Station[]> {
    return this.http.get<Station[]>(`${URL}/api/stations/all`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

}
