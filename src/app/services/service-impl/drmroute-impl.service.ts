import {DRMRouteService} from '../drmroute.service';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {DRMRoute, User} from '../../shared/entities.service';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

const URL = 'http://localhost:9006';

@Injectable({
  providedIn: 'root'
})
export class DRMRouteServiceImpl implements DRMRouteService {

  private routes = new BehaviorSubject(null);
  routeObs = this.routes.asObservable();

  constructor(private http: HttpClient) {
  }

  updateRouteList(routes: DRMRoute[]) {
    this.routes.next(routes);
  }

  save(route: DRMRoute): Observable<DRMRoute> {
    return this.http.post<DRMRoute>(`${URL}/api/routes`, route).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  update(route: DRMRoute): Observable<DRMRoute> {
    return this.http.put<DRMRoute>(`${URL}/api/routes`, route).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  fetch(id: number): Observable<DRMRoute> {
    return this.http.get<DRMRoute>(`${URL}/api/routes/${id}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${URL}/api/routes/${id}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  fetchAllByUserId(uuid: string): Observable<DRMRoute[]> {
    return this.http.get<DRMRoute[]>(`${URL}/api/routes/users/${uuid}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }
}
