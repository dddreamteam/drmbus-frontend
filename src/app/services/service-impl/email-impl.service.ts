import { Injectable } from '@angular/core';
import {EmailService} from '../email.service';
import {Observable} from 'rxjs';
import {Address, Email} from '../../shared/entities.service';
import {HttpClient} from '@angular/common/http';

const URL = 'http://localhost:9006';

@Injectable({
  providedIn: 'root'
})
export class EmailServiceImpl  implements EmailService{

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<void> {
    return undefined;
  }

  fetch(id: number): Observable<Email> {
    return undefined;
  }

  save(email: Email): Observable<Email> {
    return  this.http.post<Email>(`${URL}/api/email/`, email);
  }

  update(email: Email): Observable<Email> {
    return undefined;
  }
}
