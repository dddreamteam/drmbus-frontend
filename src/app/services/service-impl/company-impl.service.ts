import {Injectable} from '@angular/core';
import {CompanyService} from '../company.service';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {Company, CompanyDetails, DRMRoute} from '../../shared/entities.service';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

const URL = 'http://localhost:9006';

@Injectable({
  providedIn: 'root'
})
export class CompanyServiceImpl implements CompanyService {

  // company: Company;

  private company = new BehaviorSubject(null);

  compObs = this.company.asObservable();

  constructor(private http: HttpClient) {
  }


  delete: (id: number) => Observable<void>;
  fetchAllByUserUUID: (uuid: string) => Observable<Company[]>;
  update: (company: Company) => Observable<Company>;

  /* fetch: (id: number) => Observable<Company>;*/

  fetchDetailsByCompanyId(id: number): Observable<CompanyDetails> {
    return this.http.get<CompanyDetails>(`${URL}/api/companies/${id}/details`);
  }


  fetch(id: number): Observable<Company> {

    return this.http.get<Company>(`${URL}/api/companies/${id}`);

  }


  save(company: Company): Observable<Company> {
    return this.http.post<Company>(`${URL}/api/companies`, company);
  }

  saveDetails(companyDetail: CompanyDetails): Observable<CompanyDetails> {
    return this.http.post<CompanyDetails>(`${URL}/api/companies/${companyDetail.companyId}/details`, companyDetail);
  }

  fetchAllRoutesByCompanyId(id: number): Observable<DRMRoute[]> {
    return this.http.get<DRMRoute[]>(`${URL}/api/companies/${id}/routes`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

}
