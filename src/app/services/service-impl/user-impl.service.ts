
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { User } from '../../shared/entities.service';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../user.service';
import { catchError } from 'rxjs/operators';

const URL = 'http://localhost:9006';
// const URL = 'localhost:9006';

@Injectable({
  providedIn: 'root'
})
export class UserServiceImpl implements UserService {

  private user = new BehaviorSubject(null);

  userObs = this.user.asObservable();

  constructor(private http: HttpClient) {
  }

  updateUser(user: User) {
    this.user.next(user);
  }

  fetchUserByUsername(username: string): Observable<User> {
    return this.http.get<User>(`${URL}/api/users/user?username=${username}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
       // this.showErrorModal(err.error.message); // error window
        return throwError(err);
      })
    );
  }

  fetchUserById(uuid: string): Observable<User> {
    return undefined;
  }

}

