import { Injectable } from '@angular/core';
import {PhoneNumberService} from '../phone-number.service';
import {Observable} from 'rxjs';
import {PhoneNumber} from '../../shared/entities.service';
import {HttpClient} from '@angular/common/http';

const URL = 'http://localhost:9006';

@Injectable({
  providedIn: 'root'
})
export class PhoneNumberServiceImpl  implements PhoneNumberService {

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<void> {
    return undefined;
  }

  fetch(id: number): Observable<PhoneNumber> {
    return undefined;
  }

  save(phoneNumber: PhoneNumber): Observable<PhoneNumber> {
    return this.http.post<PhoneNumber>(`${URL}/api/address/`,phoneNumber);
  }

  update(phoneNumber: PhoneNumber): Observable<PhoneNumber> {
    return undefined;
  }
}
