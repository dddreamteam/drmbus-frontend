import { Injectable } from '@angular/core';
import {AddressService} from '../address.service';
import {Observable} from 'rxjs';
import {Address} from '../../shared/entities.service';
import {HttpClient} from '@angular/common/http';

const URL = 'http://localhost:9006';


@Injectable({
  providedIn: 'root'
})
export class AddressServiceImpl implements AddressService{

  constructor(private http: HttpClient) { }

  delete(id: number): Observable<void> {
    return undefined;
  }

  fetch(id: number): Observable<Address> {
    return undefined;
  }

  save(address: Address): Observable<Address> {
    return this.http.post<Address>(`${URL}/api/address`, address);
  }

  update(address: Address): Observable<Address> {
    return undefined;
  }
}
