import {ScheduleService} from '../schedule.service';
import {HttpClient} from '@angular/common/http';
import {Schedule} from '../../shared/entities.service';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';

const URL = 'http://localhost:9006';

@Injectable({
  providedIn: 'root'
})
export class ScheduleServiceImpl implements ScheduleService {

  constructor(private http: HttpClient) {
  }

  save(schedule: Schedule): Observable<Schedule> {
    return this.http.post<Schedule>(`${URL}/api/schedules`, schedule).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  update(schedule: Schedule): Observable<Schedule> {
    return this.http.put<Schedule>(`${URL}/api/schedules`, schedule).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  fetch(id: number): Observable<Schedule> {
    return this.http.get<Schedule>(`${URL}/api/schedules/${id}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${URL}/api/schedules/${id}`).pipe(
      catchError(err => {
        console.log('Error:', err.error.message);
        return throwError(err);
      })
    );
  }

}
