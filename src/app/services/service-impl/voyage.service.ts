import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Voyage } from '../../shared/entities.service';
import { HttpClient } from '@angular/common/http';

const URL = 'http://localhost:9006';
// const URL = '';

@Injectable({
  providedIn: 'root'
})
export class VoyageService {

  constructor(private http: HttpClient) { }

  fetchAllByUUID(uuid: string): Observable<Voyage[]> {
    return this.http.get<Voyage[]>(`${URL}/api/voyages/users/${uuid}`);
  }

}
