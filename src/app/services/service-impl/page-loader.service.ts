import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PageLoaderService {

  private loading = true;

  constructor() { }

  public show() {
    this.loading = true;
  }
  public hide() {
    this.loading = false;
  }
  public get getLoader(): boolean {
    return this.loading;
  }
}
