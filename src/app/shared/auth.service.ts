import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './entities.service';


const URL = 'http://localhost:9006';

@Injectable({providedIn: 'root'})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  private isAuth = false;

  getToken(body: URLSearchParams): Observable<boolean> {
    const options = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', 'Basic aW5uZXItd2ViOnNlY3I=')
    };
    return this.http.post<boolean>(
      `https://drmbus-auth-service.herokuapp.com/oauth/token`,
      body.toString(),
      options);
  }
  public login() {
    this.isAuth = true;
  }
  public logout() {
    this.isAuth = false;
  }

  public _isAuth(): boolean {
    return this.isAuth;
  }
}
