import { Injectable } from '@angular/core';

export interface Address {
  addressId?: number;
  country: string;
  zone?: string;
  city: string;
  street: string;
  building: string;
  office?: string;
  zip?: string;
}

export interface Schedule {
  id?: number;
  routeDTO: DRMRoute;
  type: string;
  interval?: number;
  monday?: boolean;
  tuesday?: boolean;
  wednesday?: boolean;
  thursday?: boolean;
  friday?: boolean;
  saturday?: boolean;
  sunday?: boolean;
}

export interface Station {
  id?: number;
  address?: Address;
}

export interface DRMRoute {
  id?: number;
  number: string;
  companyId: number;
  isActive: boolean;
  firstVoyageDate: Date;
  deepSales: number;
  seatCount: number;
  stations: Station[];

  arrivalStation?: Station;
  departureStation?: Station;
  endDate?: string;
  voyageList?: Voyage[];
}

export interface Voyage {
  id?: number;
  status?: VoyageStatus;
  route: DRMRoute;
  departureDateTime: Date;
  seats?: Seat[];
}

export interface Seat {
  id?: number;
  voyageId: number;
 /* status: SeatStatus;*/
  status: string;
}

export interface Ticket {
  id?: number;
}

export enum SeatStatus {
  AVAILABLE,
  SOLD,
  RESERVE,
  UNAVAILABLE
}

export enum VoyageStatus {
  WEEK_MORE,
  WEEK_TO,
  DAY_TO,
  ON_THE_WAY,
  CANCELED
}

export interface Message {
  text: string;
}

export interface User {
  uuid?: string;
  grant_type?: string;
  username: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  middleName?: string;
  avatarURL?: string;
  roles?: string[];
  companies?: Company[];
  companyIdToCRoleTypeMap?: Map<number, string>;
}

export interface Company {
  id?: number;
  legalForm: string;
  name: string;
  ownerUser: string;
  edrpo_inn?: string;
}
export interface CompanyDetails {
  id?: number;
  companyId: number;
  emails?: Email[];
  phoneNumbers?: PhoneNumber[];
  realAddress: Address;
}
export interface Email {
  id?: number;
  companyDetailsId?: number;
  email: string;
}

export interface PhoneNumber {
  id?: number;
  companyDetailsId?: number;
  phone: string;
}

@Injectable({
  providedIn: 'root'
})
export class EntitiesService {
  constructor() { }
}
