import { Component, OnInit } from '@angular/core';
import { UserServiceImpl } from './services/service-impl/user-impl.service';
import { PageLoaderService } from './services/service-impl/page-loader.service';
import { AuthService } from './shared/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {


  constructor(public loader: PageLoaderService,
              public authService: AuthService) { }

  ngOnInit(): void {
    this.loader.show();

  }

}
