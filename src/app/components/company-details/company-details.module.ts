import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyDetailsComponent } from './company-details.component';
import {CompanyRoutesModule} from '../../pages/company-page/company-routes/company-routes.module';
import {MaterialModule} from '../../material.module';
import {ReactiveFormsModule} from '@angular/forms';
import { ToolbarModule } from '../../base-components/toolbar/toolbar.module';


@NgModule({
  declarations: [CompanyDetailsComponent],
    imports: [
        CommonModule,
        CompanyRoutesModule,
        MaterialModule,
        ReactiveFormsModule,
        ToolbarModule
    ],
  exports: [
    CompanyDetailsComponent,
  ]
})
export class CompanyDetailsModule { }
