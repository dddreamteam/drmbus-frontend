import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Address, Company, CompanyDetails, User } from '../../shared/entities.service';
import { CompanyServiceImpl } from '../../services/service-impl/company-impl.service';
import { UserServiceImpl } from '../../services/service-impl/user-impl.service';
import { EmailServiceImpl } from '../../services/service-impl/email-impl.service';
import { PhoneNumberServiceImpl } from '../../services/service-impl/phone-number-impl.service';
import { AddressServiceImpl } from '../../services/service-impl/address-impl.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: [ './company-details.component.scss' ]
})
export class CompanyDetailsComponent implements OnInit {

  private formData;
  formCompany: FormGroup;
  public company: Company;
  public user: User;
  public containerType = [
    { type: 'FOP', name: 'Фізична особа підприємець' },
    { type: 'TOV', name: 'Товариство з обмеженою відповідальністю' }
  ];
  public onCreate = true;
  public emails = [];
  public phoneNumbers = [];
  public name: string;


  constructor(private companyService: CompanyServiceImpl,
              private userService: UserServiceImpl,
              private emailService: EmailServiceImpl,
              private phoneNumberService: PhoneNumberServiceImpl,
              private addressService: AddressServiceImpl,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    //  this.id = this.route.snapshot.paramMap.get('id');
    this.route.params.subscribe((param) => {
      if (param.id) {
        this.fetchCompany(+param.id);
        this.onCreate = false;
      }
    });

    this.formCompany = new FormGroup({
      legalForm: new FormControl('', [
        Validators.required
      ]),
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      edrpou: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(8),
      ]),
      address: new FormGroup({
        country: new FormControl('Україна', []),
        city: new FormControl(''),
        street: new FormControl(''),
        building: new FormControl(''),
        office: new FormControl('')
      }),
      emails: new FormArray([]),
      phoneNumbers: new FormArray([])
    });

  }


  submit() {
    if (this.formCompany.valid) {
      this.formData = { ...this.formCompany.value };
      this.userService.userObs.subscribe(user => {
        this.user = user;
      });
      const newCompany: Company = {
        legalForm: this.formData.legalForm,
        edrpo_inn: this.formData.edrpou,
        name: this.formData.name,
        ownerUser: this.user.uuid,
      };
      this.companyService.save(newCompany).subscribe(company => {
        this.user.companies.push(company);
        this.userService.updateUser(this.user);
        this.saveAddress(company.id);
        this.formCompany.reset();
      });
    }
  }

  addEmail() {
    const emailControl = new FormControl('', [
      Validators.email,
      Validators.required
    ]);
    (this.formCompany.get('emails') as FormArray).push(emailControl);

  }

  addPhoneNumbers() {
    const phoneNumbersControl = new FormControl('', [
      Validators.required
    ]);
    (this.formCompany.get('phoneNumbers') as FormArray).push(phoneNumbersControl);

  }

  saveCompanyDetails(idCompany: number, address: Address) {
    const companyDetails: CompanyDetails = {
      companyId: idCompany, emails: this.formData.emails, realAddress: address, phoneNumbers: this.formData.phoneNumbers
    };
    this.companyService.saveDetails(companyDetails).subscribe();
  }


  saveAddress(idCompany: number) {

    const address: Address = {
      country: this.formData.address.country,
      city: this.formData.address.city,
      street: this.formData.address.street,
      building: this.formData.address.building,
      office: this.formData.address.office,
      zip: '00000',
      zone: '3'
    };
    this.addressService.save(address).subscribe(adr => {
      this.saveCompanyDetails(idCompany, adr);
    });
  }

  fetchCompany(id: number) {
    this.companyService.fetch(id).subscribe(company => {
      this.company = company;
      this.fetchCompanyDetails(id);
      this.formCompany.patchValue({
        name: this.company.name,
        legalForm: this.company.legalForm,
        edrpou: this.company.edrpo_inn,
      });
    });
  }

  fetchCompanyDetails(id: number) {
    const obsComD: Observable<CompanyDetails> = this.companyService.fetchDetailsByCompanyId(id);
    obsComD.subscribe(comD => {
        this.formCompany.patchValue({
          address: {
            country: comD.realAddress.country,
            city: comD.realAddress.city,
            building: comD.realAddress.building,
            street: comD.realAddress.street,
            office: comD.realAddress.office,
          }
        });
        this.emails = [ ...comD.emails ];
        this.phoneNumbers = [ ...comD.phoneNumbers ];
      }
    );

  }
}

