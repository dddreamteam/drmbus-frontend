import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLoadingComponent } from './page-loading.component';
import { MaterialModule } from '../../material.module';



@NgModule({
  declarations: [ PageLoadingComponent ],
  exports: [
    PageLoadingComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ]
})
export class PageLoadingModule { }
