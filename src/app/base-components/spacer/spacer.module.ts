import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpacerComponent } from './spacer.component';

@NgModule({
  declarations: [
    SpacerComponent,
  ],
  exports: [
    SpacerComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SpacerModule { }
