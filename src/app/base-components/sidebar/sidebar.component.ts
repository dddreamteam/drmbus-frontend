import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { User } from '../../shared/entities.service';
import { UserServiceImpl } from '../../services/service-impl/user-impl.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarComponent implements OnInit {

  public user: User;
  public companyDefault = 'Select company';

  public isExpanded = true;

  constructor(private userServiceImpl: UserServiceImpl, private router: Router) { }

  ngOnInit(): void {
    this.userServiceImpl.userObs.subscribe(user => {
      this.user = user;
    });
  }

  switchCompany(id: number) {
    this.router.navigate([ '/companies', id ]);
  }
}
