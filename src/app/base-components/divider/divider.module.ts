import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DividerComponent } from './divider.component';
import { MaterialModule } from '../../material.module';



@NgModule({
  declarations: [
    DividerComponent
  ],
  exports: [
    DividerComponent
  ],
  imports: [
      CommonModule,
      MaterialModule
  ]
})
export class DividerModule { }
